package br.com.funhouse.time;

@SuppressWarnings("serial")
public class SaldoInsuficiente extends RuntimeException {
  
  public SaldoInsuficiente(){
    super("A conta corrente n�o possui saldo suficiente para a opera��o");
  }
  
}
