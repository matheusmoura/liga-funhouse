package br.com.funhouse.time;

import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.Time;

@SuppressWarnings("serial")
public class JogadorNaoPertenceAoTime extends RuntimeException {
  
  public JogadorNaoPertenceAoTime(Time time, Jogador jogador){
    super("O jogador " + jogador.getNome() + " n�o pertence ao time " + time.getNome());
  }
  
}
