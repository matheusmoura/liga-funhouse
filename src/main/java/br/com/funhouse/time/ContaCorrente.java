package br.com.funhouse.time;

public class ContaCorrente {
  
  private Double saldo;
  
  public ContaCorrente(Double saldoInicial){
    this.saldo = saldoInicial;
  }
  
  public boolean temSaldo() {
    return saldo > 0;
  }

  public void debitar(Double valor) {
    if(saldo - valor < 0){
      throw new SaldoInsuficiente();
    }
    saldo -= valor;
    
  }

  public void creditar(Integer valor) {
    saldo += valor;
  }

  public Double getSaldo() {
    return saldo;
  }

  public void setSaldo(Double saldo) {
    this.saldo = saldo;
  }
  
  
}
