package br.com.funhouse.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.Liga;
import br.com.funhouse.liga.Time;
import br.com.funhouse.mercado.Mercado;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.time.JogadorNaoPertenceAoTime;
import br.com.funhouse.time.SaldoInsuficiente;

@Resource
@Path("time")
public class TimeController {
  
  private Liga liga;
  private Time time;
  private Mercado mercado;
  private Result result;
  private HttpSession session;

  public TimeController(Liga liga, Result result, HttpSession session, Time time, Mercado mercado){
    this.liga = liga;
    this.result = result;
    this.session = session;
    this.time = time;
    this.mercado = mercado;
  }
  
  @Path("/")
  @Get
  public void index(){
    List<Time> times = liga.getTimes();
    result.include("times", times);
  }
  
  
  @Path("/{id}")
  @Get
  public void exibir(Integer id){
    Usuario usuarioLogado = (Usuario) session.getAttribute("session");
    if(usuarioLogado.getTime().getId() != id){
      result.include("error", "Voc� n�o est� autorizado a acessar �rea de outro time. Gaiato!!");
    }else{
      Time time = liga.getTime(id, false);
      result.include("time", time);
    }
  }
  
  @Post
  @Path("/incluir/jogador/{jogadorId}")
  public void incluir(Integer jogadorId) {
    Usuario usuario = (Usuario) session.getAttribute("session");
    Jogador jogador = liga.getJogador(jogadorId);
    Time time = liga.getTime(usuario.getTime().getId(), false);
    this.time.setId(time.getId());
    try {
      statusMercado();
      time.adicionar(jogador, false);
      this.time.inserir(jogador);
      result.include("notice", "Jogador "+jogador.getNome()+" inclu�do com sucesso!");
      result.redirectTo(this).exibir(time.getId());   
      return;
    }catch (SaldoInsuficiente e) {
      result.include("error", e.getMessage());
      result.redirectTo(this).exibir(time.getId());   
    }
    catch (Exception e) {
      result.include("error", e.getMessage());
      result.redirectTo(this).exibir(time.getId());   
    }
  }
  
  @Post
  @Path("/incluir/jogador/refugos/{jogadorId}")
  public void incluirRefugos(Integer jogadorId) {
    Usuario usuario = (Usuario) session.getAttribute("session");
    Jogador jogador = liga.getJogador(jogadorId);
    Time time = liga.getTime(usuario.getTime().getId(), false);
    this.time.setId(time.getId());
    try {
      statusMercado();
      time.adicionar(jogador, false);
      this.time.inserirRefugo(jogador);
      result.include("notice", "Jogador "+jogador.getNome()+" inclu�do com sucesso!");
      result.redirectTo(this).exibir(time.getId());   
      return;
    }catch (SaldoInsuficiente e) {
      result.include("error", e.getMessage());
      result.redirectTo(this).exibir(time.getId());   
    }
    catch (Exception e) {
      result.include("error", e.getMessage());
      result.redirectTo(this).exibir(time.getId());   
    }
  }

  private void statusMercado() throws Exception{
    String status = mercado.getStatus();
    if(!status.equals("ABERTO") && !status.equals("REFUGOS")){
      throw new Exception("O mercado est� com status "+status+"!!");
    }
  }


  @Path("/excluir/jogador/{jogadorId}")
  public void excluir(Integer jogadorId) {
    Time time = preencheTime();
    Jogador jogador = liga.getJogador(jogadorId);
    
    try {
      statusMercado();
      this.time.excluir(jogador);
      result.include("notice", "Jogador "+jogador.getNome()+" exclu�do com sucesso!");
      result.redirectTo(this).exibir(time.getId());   
      return;
    }
    catch (JogadorNaoPertenceAoTime e) {
      result.include("error", e.getMessage());
      result.redirectTo(this).exibir(time.getId());   
    }catch (Exception e) {
      result.include("error", e.getMessage());
      result.redirectTo(this).exibir(time.getId());   
    }
  }

  private Time preencheTime() {
    Usuario user = (Usuario)session.getAttribute("session");
    Time time = liga.getTime(user.getTime().getId(), false);
    this.time.setId(time.getId());
    this.time.setNome(time.getNome());
    this.time.setJogadores(time.getJogadores());
    return time;
  }

  @Path("listar")
  public void listar(){
    Usuario usuario = (Usuario) session.getAttribute("session");
    String status = mercado.getStatus();
    if(!status.equals("FINALIZADO")){
      result.include("notice", "Sess�i indispon�vel no momento pois o mercado est� com status: "+status)
                    .redirectTo(this)
                    .exibir(usuario
                                .getTime()
                                .getId());
    }
    result.include("times", time.todos());
  }
  
  @Post
  @Path("/pesquisa")
  public void pesquisa(Time time){
    result.include("time", liga.getTime(time.getId(), false)).redirectTo(this).listar();
  }
  
  
  @Post
  @Path("/editar")
  public void editar(List<Time> times){
    time.atualizar(times);
    result.include("notice", "Caixa ajustado com sucesso!").redirectTo(AdministrativoController.class).index();
  }
}
