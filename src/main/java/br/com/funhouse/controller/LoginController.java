package br.com.funhouse.controller;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.encoding.PasswordEncoder;

import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.funhouse.liga.Liga;
import br.com.funhouse.liga.Time;
import br.com.funhouse.liga.TimeService;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.UsuarioService;
import br.com.funhouse.security.annotation.Public;

@Resource
public class LoginController {
  
  private UsuarioService service;
  private Liga liga;
  private Result result;
  private PasswordEncoder encoder;
  private HttpSession session;
  
  public LoginController(HttpSession session, UsuarioService service, 
                         Result result, PasswordEncoder encoder, Liga ligaService) {
    
    this.session = session;
    this.service = service;
    this.result = result;
    this.encoder = encoder;
    this.liga = ligaService;
  }
  
  @Public
  @Post("/autenticar")
  public void autenticar(Usuario usuario) {
    usuario.setSenha(encoder.encodePassword(usuario.getSenha(), null));
    Usuario user = service.getUsuarioPorLoginESenha(usuario);
    if (user != null) {
      
      Time time = liga.getTime(user.getTime().getId(), false);
      session.setAttribute("emblema", time.getEmblema());
      session.setAttribute("session", user);
      result.redirectTo(TimeController.class)
            .exibir(user.getTime().getId());
    }
    else {
      result.include("error", "Login ou senha incorreto!")
            .redirectTo(IndexController.class)
            .index();
    }
  }
  
  @Get("/logout")
  @Public
  public void logout() {
    session.invalidate();
    result.redirectTo(IndexController.class).index();
  }
  
}
