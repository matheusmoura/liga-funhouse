package br.com.funhouse.controller;

import javax.servlet.http.HttpSession;

import org.springframework.security.authentication.encoding.PasswordEncoder;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Put;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.UsuarioService;

@Resource
@Path("usuario")
public class UsuarioController {

  private UsuarioService service;
  private HttpSession session;
  private Validator validator;
  private Result result;
  private PasswordEncoder encoder;
  
  public UsuarioController(UsuarioService service, HttpSession session, Validator validator, Result result, PasswordEncoder encoder){
    this.service = service;
    this.session = session;
    this.validator = validator; 
    this.result = result;
    this.encoder = encoder;
  }
  
  public void salvar(Usuario usuario) {
    service.salvar(usuario);
  }

  @Put
  @Path("/alterarSenha")
  public void alterarSenha(final Usuario usuario) {
    Validations validations = new Validations(){{
      that(!usuario.getSenha().isEmpty(), "senhaInvalida", "Informe uma senha!");
    }};

    validator.checking(validations);
    validator.onErrorUsePageOf(this).index();
    
    Usuario usuarioSessao = (Usuario) session.getAttribute("session");
    usuario.setId(usuarioSessao.getId());
    usuario.setSenha(encoder.encodePassword(usuario.getSenha(), null));
    
    service.atualizar(usuario);
    result.include("notice", "Senha alterada com sucesso!");
    result.redirectTo(this).index();
  }
  
  @Path("/index")
  public void index(){
    
  }
  
}
