package br.com.funhouse.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.funhouse.liga.Liga;
import br.com.funhouse.liga.Time;
import br.com.funhouse.mercado.Leilao;
import br.com.funhouse.mercado.Mercado;
import br.com.funhouse.security.Usuario;


@Resource
@Path("leilao")
public class LeilaoController {

  private Result result;
  private Liga liga;
  private Time time;
  private HttpSession session;
  private Mercado mercado;
  
  public LeilaoController(Result result, Liga liga, HttpSession session, Mercado mercado, Time time) {
    this.result = result;
    this.liga = liga;
    this.session = session;
    this.mercado = mercado;
    this.time = time;
  }

  
  @Path("/index")
  public void index(){
    Usuario usuarioLogado = (Usuario)session.getAttribute("session");
    if(!mercado.getStatus().equals("EM LEIL�O")){
      result.include("error", "O mercado n�o est� em Leil�o no momento.");
      result.redirectTo(TimeController.class).exibir(usuarioLogado.getTime().getId());
      return;
    } 
    Time timeEscalado = liga.getTime(usuarioLogado.getTime().getId(), false);
    result.include("saldo", timeEscalado.getConta().getSaldo());
    result.include("jogadoresEmLeilao", time.listarJogadoresEmLeilao(usuarioLogado.getTime().getId()));
  }

  
  @Post
  @Path("/enviar")
  public void enviar(List<Leilao> leilao){
    Usuario usuarioLogado = (Usuario)session.getAttribute("session");
    try {
      time.setId(usuarioLogado.getTime().getId());
      zerarLance(leilao);
      time.enviarLance(leilao);
    }
    catch (Exception e) {
      result.include("error", e.getMessage()).redirectTo(this).index();
      return;
    }
    result.include("notice", "Lance enviado com sucesso!").redirectTo(this).index();
  }

	private void zerarLance(List<Leilao> leiloes) {
		for (Leilao leilao : leiloes) {
			if(leilao.getLance() == null){
				leilao.setLance(0D);
			}
		}
	}
}
