package br.com.funhouse.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.Liga;
import br.com.funhouse.liga.Time;
import br.com.funhouse.security.Usuario;

@Resource
@Path("/jogadores")
public class JogadorController {

  public static final String JOGADOR_NAO_ENCONTRADO = "Jogador n�o encontrado!";
  
  private Result result;
  private HttpSession session;
  private Liga liga;
  
  public JogadorController(Result result, HttpSession session, Liga liga) {
    this.result = result;
    this.session = session;
    this.liga = liga;
  }
  
  @Path("/")
  public void index(){
    Usuario usuarioLogado = (Usuario)session.getAttribute("session");
    Time time = liga.getTime(usuarioLogado.getTime().getId(), false);
    result.include("time", time);
  }
  
  @Post
  @Path("/pesquisar")
  public void pesquisar(Jogador jogador) {
    List<Jogador> jogadores = liga.pesquisarJogador(jogador);
    if(jogadores.isEmpty()){
      result.include("error", JOGADOR_NAO_ENCONTRADO);
    }
    result.include("jogadores", jogadores);
    result.redirectTo(this).index();
  }
  
}
