package br.com.funhouse.controller;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Resource;
import br.com.funhouse.security.annotation.Public;

@Resource
public class IndexController {
  
  
  @Public
  @Path("/")
  public void index() {
    
  }
  
}
