package br.com.funhouse.controller;

import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Resource;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.validator.Validations;
import br.com.funhouse.liga.Time;
import br.com.funhouse.mercado.Mercado;
import br.com.funhouse.security.annotation.Administrador;

@Resource
@Path("administrativo")
public class AdministrativoController {
  
  private Result result;
  private Mercado mercado;
  private Validator validator;
  private Time time;
  
  public AdministrativoController(Result result, Mercado mercado, Validator validator, Time time) {
    this.result = result;
    this.mercado = mercado;
    this.validator = validator;
    this.time = time;
  }

  @Administrador
  @Path("/index")
  public void index(){
    
  }
  
  @Administrador
  @Path("/mercado")
  @Post
  public void mercado(final Integer status){
    Validations validations = new Validations(){{
      that(!status.equals(99), "error", "Informe um valor para o mercado!!");
    }};
    validator.checking(validations);
    validator.onErrorUsePageOf(this).index();
    
    this.mercado.atualizaStatus(status);
    
    String statusMercado = mercado.getStatus();
    if(statusMercado.equals("REFUGOS") || statusMercado.equals("FINALIZADO")){
      mercado.atualizarEscalacaoGeral();
    }
    result.include("notice", "Mercado atualizado com sucesso!");
    result.redirectTo(this).index();
  }
  
  @Administrador
  @Path("/tabs/tab2")
  public void tab2(){
    result.include("times", time.todos());
  }
  
  @Administrador
  @Path("/tabs/tab1")
  public void tab1(){
  }
  
}
