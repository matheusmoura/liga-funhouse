package br.com.funhouse.data.mapper;

import java.util.List;
import java.util.Map;

import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.Notificacao;
import br.com.funhouse.liga.Time;
import br.com.funhouse.mercado.Leilao;

public interface TimeMapper {

  List<Time> todos();

  Time carregar(Integer id);

  List<Jogador> carregarJogadores(Integer id);

  void excluir(Map<String, Object> map);

  void adicionar(Map<String, Object> map);

  Integer statusMercado();

  List<Leilao> listarJogadoresEmLeilao(Integer idTime);

  void arremate(Leilao leilao);

  List<Leilao> listaDoJogadorEmLeilao(Integer idJogador);

  void gravarNotificacao(Notificacao notificacao);

  void atualizarEscalacao(Leilao leilao);

  void inserirRefugo(Jogador jogador);

  List<Jogador> obtemJogadorEscalado(Jogador jogador);

  List<Leilao> listarJogadoresLeilaoFinalizado();

  Double carregarValorDeCaixa(Integer timeId);

  void atualizar(Time time);
  
}
