package br.com.funhouse.data.mapper;

import java.util.List;

import br.com.funhouse.liga.Jogador;

public interface JogadorMapper {

  public List<Jogador> listJogadores(Jogador jogador);
  
  public Jogador getJogador(Jogador jogador);
  
}
