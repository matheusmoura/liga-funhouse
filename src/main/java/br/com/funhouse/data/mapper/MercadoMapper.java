package br.com.funhouse.data.mapper;

import java.util.List;

import br.com.funhouse.liga.Notificacao;
import br.com.funhouse.mercado.Leilao;

public interface MercadoMapper {

  public Integer statusMercado();

  public void atualizaStatus(Integer id);

  public List<Notificacao> listarNotificacoes();

  public List<Leilao> listarJogadoresLeilaoFinalizado();

  public List<Notificacao> listaNotificacoesDeUmJogador(String string);
  
}
