package br.com.funhouse.data.mapper;

import org.springframework.security.core.userdetails.UserDetails;

import br.com.funhouse.security.Usuario;

public interface UsuarioMapper {

  public void salvar(Usuario usuario);

  public Usuario getUsuarioPorLoginESenha(Usuario usuario);

  public void atualizar(Usuario usuario);

  public UserDetails usuarioPeloLogin(String username);

}
