package br.com.funhouse.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.funhouse.controller.IndexController;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.annotation.Public;

@Intercepts
public class LoginInterceptor implements Interceptor {
  
  private Result result;
  private HttpSession session;
  
  public LoginInterceptor(Result result, HttpSession session) {
    this.result = result;
    this.session = session;
  }
  
  @Override
  public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance) {
    Usuario user = (Usuario) session.getAttribute("session");
    if (user != null) {
      stack.next(method, resourceInstance);
    }else{
      result.include("error", "� preciso se logar para acessar esta p�gina!")
            .redirectTo(IndexController.class)
            .index();
    }
  }
  
  @Override
  public boolean accepts(ResourceMethod method) throws InterceptionException {
    Method metodo = method.getMethod();
    Class<?> resource = method.getResource().getType();
    
    return !(metodo.isAnnotationPresent(Public.class) || resource.isAnnotationPresent(Public.class));
  }
  
}
