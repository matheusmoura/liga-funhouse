package br.com.funhouse.interceptor;

import java.lang.reflect.Method;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.funhouse.mercado.Mercado;
import br.com.funhouse.security.annotation.Public;

@Intercepts
public class MercadoInterceptor implements Interceptor{

  private Result result;
  private Mercado mercado;
  
  public MercadoInterceptor(Mercado mercado, Result result){
    this.mercado = mercado;
    this.result = result;
  }
  
  @Override
  public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance)
      throws InterceptionException {
    result.include("statusMercado", mercado.getStatus());
    result.include("notificacoes", mercado.listarNotificacoes());
    stack.next(method, resourceInstance);
  }

  @Override
  public boolean accepts(ResourceMethod method) {
    Method metodo = method.getMethod();
    Class<?> resource = method.getResource().getType();
    
    return !(metodo.isAnnotationPresent(Public.class) || resource.isAnnotationPresent(Public.class));
  }
  
}
