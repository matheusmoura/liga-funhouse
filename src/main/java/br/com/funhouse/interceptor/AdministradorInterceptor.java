package br.com.funhouse.interceptor;

import java.lang.reflect.Method;

import javax.servlet.http.HttpSession;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.caelum.vraptor.resource.ResourceMethod;
import br.com.funhouse.controller.TimeController;
import br.com.funhouse.liga.Time;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.annotation.Administrador;

@Intercepts
public class AdministradorInterceptor implements Interceptor{
  
  private HttpSession session;
  private Result result;
  
  public AdministradorInterceptor(HttpSession session, Result result) {
    this.session = session;
    this.result = result;
  }

  @Override
  public void intercept(InterceptorStack stack, ResourceMethod method, Object resourceInstance)
      throws InterceptionException {
    Usuario user = (Usuario) session.getAttribute("session");
    if (user != null && user.getAdministrador() == 1) {
      stack.next(method, resourceInstance);
    }else{
      result.include("error", "Voc� n�o tem privil�gios para acessar esta p�gina!")
            .redirectTo(TimeController.class)
            .exibir(user.getTime().getId());
    }
  }

  @Override
  public boolean accepts(ResourceMethod method) {
    Method metodo = method.getMethod();
    Class<?> resource = method.getResource().getType();
    return (metodo.isAnnotationPresent(Administrador.class) || resource.isAnnotationPresent(Administrador.class));
  }
  
}
