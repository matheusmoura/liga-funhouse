package br.com.funhouse.liga;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.funhouse.mercado.Leilao;
import br.com.funhouse.time.ContaCorrente;
import br.com.funhouse.time.JogadorNaoPertenceAoTime;
import br.com.funhouse.time.SaldoInsuficiente;
import br.com.funhouse.time.TimeCompleto;
@Service
@ApplicationScoped
public class Time {
  
  private Integer id;
  private String nome;
  private Double caixa;
  private String emblema;
  private ContaCorrente conta;
  private List<Jogador> jogadores;
  
  private TimeService service;
  private Liga liga;
  
  @Autowired
  public void setService(TimeService service) {
    this.service = service;
  }
  
  @Autowired
  public void setLiga(Liga liga) {
    this.liga = liga;
  }
  
  public Time() {
    jogadores = new ArrayList<Jogador>();
  }
  
  public Time(ContaCorrente conta) {
    this.conta = conta;
  }
 
  
  public Integer quantidadeDeJogadoresEscalados() {
    return jogadores.size();
  }
  
  public void adicionar(Jogador jogador, Boolean saldoLeilao){
    if(!conta.temSaldo()){
      throw new SaldoInsuficiente();
    }
    if(quantidadeDeJogadoresEscalados() == 22){
      throw new TimeCompleto();
    }
    conta.debitar(jogador.getPreco().doubleValue());
    if(saldoLeilao == false){
    	conta.debitar(jogador.getValorLeilao()==null?0:jogador.getValorLeilao());
    }
    jogadores.add(jogador);
  }


  public void inserir(Jogador jogador) throws Exception{
    Map<String, Object> map = adicionaParametrosCrud(jogador);
    try {
      map.put("refugo", 0);
      service.adicionar(map);
    }
    catch (Exception e) {
      throw new Exception("Jogador "+jogador.getNome()+" j� est� incluso no seu time!");
    }
  }

  private Map<String, Object> adicionaParametrosCrud(Jogador jogador) {
    Map<String, Object> map = new HashMap<String, Object>();
    map.put("timeId", this.id);
    map.put("data", new Date());
    map.put("jogadorId", jogador.getId());
    return map;
  }
  
  public void excluir(Jogador jogador) {
    if(!jogadores.contains(jogador)){
      throw new JogadorNaoPertenceAoTime(this, jogador);
    }
    Map<String, Object> map = adicionaParametrosCrud(jogador);
    service.excluir(map);
  }
  
  public List<Leilao> listarJogadoresEmLeilao(Integer idTime) {
   return service.listarJogadoresEmLeilao(idTime); 
  }

  public void enviarLance(List<Leilao> leilao) throws Exception {
    Time timeCarregado = liga.getTime(getId(), true);
    verificaValoresLeilao(leilao, timeCarregado);
    service.arremate(leilao);
  }

  private void verificaValoresLeilao(List<Leilao> leilao, Time timeCarregado) throws Exception {
    double soma = 0;
    for (Leilao l : leilao) {
      soma += l.getLance();
      l.setIdTime(timeCarregado.getId());
    }
    double saldo = timeCarregado.getConta().getSaldo();
    for (Jogador  j : timeCarregado.getJogadores()) {
		if (!isJogadorEmLeilao(j, timeCarregado.getId())) 
			saldo -= j.getValorLeilao();
	}
    
    if(soma > saldo){
      throw new Exception("Saldo insuficiente!");
    }
  }

  private boolean isJogadorEmLeilao(Jogador j, Integer idTime) {
	List<Leilao> listarJogadoresEmLeilao = service.listarJogadoresEmLeilao(idTime);
	for (Leilao leilao : listarJogadoresEmLeilao) {
		if(j.getId().equals(leilao.getIdJogador())){
			return true;
		}
	}
	return false;
}

public void inserirRefugo(Jogador jogador) throws Exception  {
    Map<String, Object> map = adicionaParametrosCrud(jogador);
    map.put("refugo", 1);
    List<Jogador> jogadorEscalado = service.obtemJogadorEscalado(jogador);
    if(!jogadorEscalado.isEmpty()){
       throw new Exception("O jogador "+jogador.getNome()+" j� est� escalado em outro time!");
    }
    service.adicionar(map);
  }

  public List<Time> todos() {
    return service.todos();
  }
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getNome() {
    return nome;
  }
  
  public void setNome(String nome) {
    this.nome = nome;
  }
  
  public Integer getValor(){
    return 0;
  }
  public Double getCaixa() {
    return caixa;
  }

  public void setCaixa(Double caixa) {
    this.caixa = caixa;
  }
  public List<Jogador> getJogadores(){
    return jogadores;
  }

  public void setJogadores(List<Jogador> jogadores) {
   this.jogadores = jogadores;
  }

  public ContaCorrente getConta() {
    return conta;
  }

  public void setConta(ContaCorrente conta) {
    this.conta = conta;
  }

  public void atualizar(List<Time> times) {
    service.atualizar(times);
  }
  public String getEmblema() {
	return emblema;
  }

  public void setEmblema(String emblema) {
	 this.emblema = emblema;
  }

}
