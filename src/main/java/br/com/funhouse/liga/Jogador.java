package br.com.funhouse.liga;


public class Jogador {
  
  private Integer id;
  private String nome;
  private String nomeTime;
  private String posicao;
  private Integer preco;
  private Double valorLeilao;
  private Integer escalado;
  private Integer refugo;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getPosicao() {
    return posicao;
  }
  
  public void setPosicao(String posicao) {
    this.posicao = posicao;
  }
  
  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getNomeTime() {
    return nomeTime;
  }

  public void setNomeTime(String nomeTime) {
    this.nomeTime = nomeTime;
  }

  public Integer getPreco() {
    return preco;
  }

  public void setPreco(Integer preco) {
    this.preco = preco;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Jogador other = (Jogador) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    }
    else if (!id.equals(other.id))
      return false;
    return true;
  }

  public Double getValorLeilao() {
    return valorLeilao;
  }

  public void setValorLeilao(Double valorLeilao) {
    this.valorLeilao = valorLeilao;
  }

  public Integer getEscalado() {
    return escalado;
  }

  public void setEscalado(Integer escalado) {
    this.escalado = escalado;
  }

  public Integer getRefugo() {
    return refugo;
  }

  public void setRefugo(Integer refugo) {
    this.refugo = refugo;
  }
  
}
