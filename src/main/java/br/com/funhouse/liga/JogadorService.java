package br.com.funhouse.liga;

import java.util.List;


public interface JogadorService {
  
  public List<Jogador> listJogadores(Jogador jogador);

  public Jogador getJogador(Jogador jogador);

  public List<Jogador> pesquisar(Jogador jogador);

  public List<Jogador> todos();

  public Jogador carregar(Integer jogadorId);
  
}
