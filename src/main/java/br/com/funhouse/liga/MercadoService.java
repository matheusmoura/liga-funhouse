package br.com.funhouse.liga;

import java.util.List;

public interface MercadoService {
  
  Integer statusMercado();

  void atualizaStatus(Integer id);

  List<Notificacao> listarNotificacoes();

  void atualizarEscalacaoGeral();
  
}
