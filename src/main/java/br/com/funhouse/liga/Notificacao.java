package br.com.funhouse.liga;

import java.util.Date;

public class Notificacao {
  
  private Integer id;
  private String mensagem;
  private Date data;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getMensagem() {
    return mensagem;
  }
  
  public void setMensagem(String mensagem) {
    this.mensagem = mensagem;
  }

  public Date getData() {
    return data;
  }

  public void setData(Date data) {
    this.data = data;
  }
  
}
