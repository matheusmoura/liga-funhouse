package br.com.funhouse.liga;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.caelum.vraptor.ioc.ApplicationScoped;

@Service
@ApplicationScoped
public class Liga {
  
  private JogadorService jogadorService;
  private TimeService timeService;
  
  @Autowired
  public Liga(JogadorService jogadorService, TimeService timeService){
    this.jogadorService = jogadorService;
    this.timeService = timeService;
  }
  
  public List<Jogador> getJogadores(){
    return jogadorService.todos();
  }

  public List<Jogador> pesquisarJogador(Jogador jogador) {
    return jogadorService.pesquisar(jogador);
  }

  public Time getTime(Integer timeId, Boolean saldoLeilao) {
    Time time = timeService.carregar(timeId, saldoLeilao);
    return time;
  }

  public Jogador getJogador(Integer jogadorId) {
    return jogadorService.carregar(jogadorId);
  }

  public List<Time> getTimes() {
    return timeService.todos();
  }
}
