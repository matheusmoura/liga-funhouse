package br.com.funhouse.liga.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.funhouse.data.mapper.MercadoMapper;
import br.com.funhouse.data.mapper.TimeMapper;
import br.com.funhouse.liga.MercadoService;
import br.com.funhouse.liga.Notificacao;
import br.com.funhouse.mercado.Leilao;

@Service
public class MercadoServiceImpl implements MercadoService{

  private MercadoMapper mapper;
  private TimeMapper timeMapper;
  
  @Autowired
  public void setMapper(MercadoMapper mapper, TimeMapper timeMapper) {
    this.mapper = mapper;
    this.timeMapper = timeMapper;
  }
  
  public Integer statusMercado() {
    return mapper.statusMercado();
  }

  @Override
  public void atualizaStatus(Integer id) {
    mapper.atualizaStatus(id);
  }

  @Override
  public List<Notificacao> listarNotificacoes() {
    return mapper.listarNotificacoes();
  }

  @Override
  public void atualizarEscalacaoGeral() {
    List<Leilao> jogadoresRetorno = mapper.listarJogadoresLeilaoFinalizado();
    List<Leilao> jogadoresPerdedores =  new ArrayList<Leilao>();
    List<Leilao> jogadoresVencedores = new ArrayList<Leilao>();
    gerenciarListasDeAtualizacoes(jogadoresRetorno, jogadoresPerdedores, jogadoresVencedores);
    atualizarEscalacao(jogadoresPerdedores);
    gravarNotificacao(jogadoresVencedores);
  }

  private void gravarNotificacao(List<Leilao> jogadoresVencedores) {
    for (Leilao leilao : jogadoresVencedores) {
      String jogador = leilao.getNomeJogador();
      
      List<Notificacao> jogadorNotificado = mapper.listaNotificacoesDeUmJogador("%"+jogador+"%");
      if (jogadorNotificado.isEmpty()) {
        String mensagem = "Ihhhh que vacilo! O leil�o acabou e o esqueceram de dar o lance pro "
          +leilao.getNomeJogador()
          +"!! Bom pro time "+leilao.getNomeTime()+" que levou o jogador com o lance de "+leilao.getLance()+"!!";
        
        Notificacao notificacao = new Notificacao();
        notificacao.setData(new Date());
        notificacao.setMensagem(mensagem);
        timeMapper.gravarNotificacao(notificacao);
      }
    }
  }

  private void atualizarEscalacao(List<Leilao> jogadoresPerdedores) {
    for (Leilao leilao : jogadoresPerdedores) {
      timeMapper.atualizarEscalacao(leilao);
    }
  }

  private void gerenciarListasDeAtualizacoes(List<Leilao> jogadoresRetorno,
      List<Leilao> jogadoresPerdedores, List<Leilao> jogadoresVencedores) {
    for (Leilao leilao : jogadoresRetorno) {
      if(!jogadoresVencedores.contains(leilao)){
        jogadoresVencedores.add(leilao);
      }else{
        jogadoresPerdedores.add(leilao);
      }
    }
  }
}
