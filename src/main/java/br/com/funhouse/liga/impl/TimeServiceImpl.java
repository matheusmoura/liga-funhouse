package br.com.funhouse.liga.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.funhouse.data.mapper.TimeMapper;
import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.Notificacao;
import br.com.funhouse.liga.Time;
import br.com.funhouse.liga.TimeService;
import br.com.funhouse.mercado.Leilao;
import br.com.funhouse.time.ContaCorrente;
@Service
public class TimeServiceImpl implements TimeService {
  
  private TimeMapper mapper;
  
  @Autowired
  public TimeServiceImpl(TimeMapper mapper){
    this.mapper = mapper;
  }
  
  @Override
  public Time carregar(Integer id, Boolean saldoLeilao) {
    Time time = mapper.carregar(id);
    ContaCorrente conta = new ContaCorrente(carregarValorDeCaixa(id));
    time.setConta(conta);
    List<Jogador> jogadores  = mapper.carregarJogadores(id);
    for (Jogador jogador : jogadores) {
      time.adicionar(jogador, saldoLeilao);
    }
    return time;
  }
  
  @Override
  public List<Time> todos() {
    return mapper.todos();
  }

  @Override
  public void excluir(Map<String, Object> map) {
    mapper.excluir(map);
  }

  @Override
  public void adicionar(Map<String, Object> map) {
    mapper.adicionar(map);
  }

  @Override
  public List<Leilao> listarJogadoresEmLeilao(Integer idTime) {
   return mapper.listarJogadoresEmLeilao(idTime);
  }

  @Override
  public void arremate(List<Leilao> leiloes) {
    gravarArremate(leiloes);
  }

  private void gravarArremate(List<Leilao> leiloes) {
    for (Leilao leilao : leiloes) {
      atualizarEscalacaoDoJogador(leilao);
      }
  }

  private void atualizarEscalacaoDoJogador(Leilao leilao) {
    List<Leilao> listaDoJogadorLeiloado = mapper.listaDoJogadorEmLeilao(leilao.getIdJogador());
    if(listaDoJogadorLeiloado.size()>=2){
      mapper.arremate(leilao); 
      listaDoJogadorLeiloado = mapper.listaDoJogadorEmLeilao(leilao.getIdJogador());
      if(listaDoJogadorLeiloado.get(0).getLance() > 0 ){
          for (int i = 0; i < listaDoJogadorLeiloado.size()-1; i++) {
              Leilao l = listaDoJogadorLeiloado.get(i);
              l.setEscalado(0);
              mapper.atualizarEscalacao(l);
            }
            gravacaoDaNotificao(listaDoJogadorLeiloado);
          }
    }
  }

  private void gravacaoDaNotificao(List<Leilao> listaDoJogadorLeiloado) {
    Leilao vencedor = listaDoJogadorLeiloado.get(listaDoJogadorLeiloado.size()-1);
    Notificacao notificacao = new Notificacao();
    notificacao.setData(new Date());
    notificacao.setMensagem("O jogador "+vencedor.getNomeJogador()+" fechou com o time "+vencedor.getNomeTime()+" pelo lance de "+ " F$ "+ vencedor.getLance()+" !!");
    mapper.gravarNotificacao(notificacao);
  }

  public List<Jogador> obtemJogadorEscalado(Jogador jogador){
    return mapper.obtemJogadorEscalado(jogador);
  }

  @Override
  public Double carregarValorDeCaixa(Integer timeId) {
    return mapper.carregarValorDeCaixa(timeId);
  }

  @Override
  public void atualizar(List<Time> times) {
    for (Time time : times) {
      mapper.atualizar(time);
    }
  }
}
