package br.com.funhouse.liga.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.funhouse.data.mapper.JogadorMapper;
import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.JogadorService;
@Service
public class JogadorServiceImpl implements JogadorService{

  private JogadorMapper mapper;
  
  @Autowired
  public void setMapper(JogadorMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public List<Jogador> listJogadores(Jogador jogador) {
    if(jogador.getNome() != ""){
      StringBuilder nome = new StringBuilder();
      nome.append("%"+jogador.getNome()+"%");
      jogador.setNome(nome.toString());
    }
    if(jogador.getPosicao() != ""){
      StringBuilder posicao = new StringBuilder();
      posicao.append("%"+jogador.getPosicao()+"%");
      jogador.setPosicao(posicao.toString());
    }
    return mapper.listJogadores(jogador);
  }

  @Override
  public Jogador getJogador(Jogador jogador) {
    return mapper.getJogador(jogador);
  }

  @Override
  public List<Jogador> pesquisar(Jogador jogador) {
    formataPosicaoJogador(jogador);
    formataNomeJogador(jogador);
    return mapper.listJogadores(jogador);
  }

  private void formataNomeJogador(Jogador jogador) {
    StringBuilder formatador = new StringBuilder();
    formatador.append("%");
    formatador.append(jogador.getNome());
    formatador.append("%");
    jogador.setNome(formatador.toString());
  }

  private void formataPosicaoJogador(Jogador jogador) {
    StringBuilder formatador = new StringBuilder();
    formatador.append("%");
    formatador.append(jogador.getPosicao());
    formatador.append("%");
    jogador.setPosicao(formatador.toString());
  }

  @Override
  public List<Jogador> todos() {
    return mapper.listJogadores(new Jogador());
  }

  @Override
  public Jogador carregar(Integer jogadorId) {
    Jogador jogador = new Jogador();
    jogador.setId(jogadorId);
    return mapper.getJogador(jogador);
  }
  
}
