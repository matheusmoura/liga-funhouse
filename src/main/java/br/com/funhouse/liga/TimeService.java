package br.com.funhouse.liga;

import java.util.List;
import java.util.Map;

import br.com.funhouse.mercado.Leilao;


public interface TimeService {

  Time carregar(Integer timeId, Boolean saldoLeilao);

  List<Time> todos();

  void excluir(Map<String, Object> map);

  void adicionar(Map<String, Object> map);

  List<Leilao> listarJogadoresEmLeilao(Integer idTime);

  void arremate(List<Leilao> leilao);
  
  public List<Jogador> obtemJogadorEscalado(Jogador jogador);

  Double carregarValorDeCaixa(Integer timeId);

  void atualizar(List<Time> times);
}
