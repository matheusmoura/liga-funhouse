package br.com.funhouse.mercado;

import java.util.Date;

public class Leilao {
  
  private Integer idJogador;
  private String nomeJogador;
  private Integer idTime;
  public String nomeTime;
  private Double lance;
  private Integer escalado;
  private Date dataEscalacao;
  
  public Integer getIdJogador() {
    return idJogador;
  }
  
  public void setIdJogador(Integer idJogador) {
    this.idJogador = idJogador;
  }
  
  public String getNomeJogador() {
    return nomeJogador;
  }
  
  public void setNomeJogador(String nomeJogador) {
    this.nomeJogador = nomeJogador;
  }
  
  public Integer getIdTime() {
    return idTime;
  }
  
  public void setIdTime(Integer idTime) {
    this.idTime = idTime;
  }
  
  public Double getLance() {
    return lance;
  }
  
  public void setLance(Double lance) {
    this.lance = lance;
  }

  public Integer getEscalado() {
    return escalado;
  }

  public void setEscalado(Integer escalado) {
    this.escalado = escalado;
  }

  public String getNomeTime() {
    return nomeTime;
  }

  public void setNomeTime(String nomeTime) {
    this.nomeTime = nomeTime;
  }

  public Date getDataEscalacao() {
    return dataEscalacao;
  }

  public void setDataEscalacao(Date dataEscalacao) {
    this.dataEscalacao = dataEscalacao;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((idJogador == null) ? 0 : idJogador.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Leilao other = (Leilao) obj;
    if (idJogador == null) {
      if (other.idJogador != null)
        return false;
    }
    else if (!idJogador.equals(other.idJogador))
      return false;
    return true;
  }


}
