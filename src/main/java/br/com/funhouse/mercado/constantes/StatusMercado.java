package br.com.funhouse.mercado.constantes;

public enum StatusMercado {
  ABERTO(0, "Aberto"),
  FECHADO(1, "Fechado"),
  EM_LEILAO(2, "Em Leil�o");
  
  private Integer id;
  private String status;
  
  StatusMercado(Integer id, String status){
    this.id = id;
    this.status = status;
  }
  
  public Integer getId() {
    return id;
  }

  public String getStatus() {
    return status;
  }
  
  
  
}
