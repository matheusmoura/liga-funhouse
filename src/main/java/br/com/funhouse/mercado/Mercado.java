package br.com.funhouse.mercado;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.caelum.vraptor.ioc.ApplicationScoped;
import br.com.funhouse.liga.MercadoService;
import br.com.funhouse.liga.Notificacao;
@Service
@ApplicationScoped
public class Mercado {
  
  private Integer id;
  
  private MercadoService service;

  @Autowired
  public void setService(MercadoService service) {
    this.service = service;
  }
  
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getStatus() {
    
    Integer status = service.statusMercado();
    
    switch (status) {
      case 0:
        return "ABERTO";
      case 1:
        return "EM LEIL�O";
      case 2:
        return "REFUGOS";
      case 3:
        return "FINALIZADO";
      default:
        return "N/A";
    }
  }

  public void atualizaStatus(Integer id){
    service.atualizaStatus(id);
  }
 
  public List<Notificacao> listarNotificacoes(){
    return service.listarNotificacoes();
  }


    public void atualizarEscalacaoGeral() {
      service.atualizarEscalacaoGeral();
  }
  
}
