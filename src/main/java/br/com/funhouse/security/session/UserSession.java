package br.com.funhouse.security.session;


import br.com.funhouse.liga.Time;
import br.com.funhouse.security.Usuario;
public class UserSession {
  
  private Usuario user;
  
  public UserSession(Usuario usuario){
    this.user = usuario;
  }
  
  public boolean isLogged(){
    return user != null;
  }

  public Usuario getUser() {
    return user;
  }
  
  public Time getTime(){
    return user.getTime();
  }
}
