package br.com.funhouse.security;


import org.springframework.security.core.userdetails.UserDetailsService;

public interface UsuarioService extends UserDetailsService{
  
  public void salvar(Usuario usuario);
  
  public Usuario getUsuarioPorLoginESenha(Usuario usuario);

  public void atualizar(Usuario usuario);
  
}
