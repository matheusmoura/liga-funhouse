package br.com.funhouse.security.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.caelum.vraptor.ioc.Component;
import br.com.funhouse.data.mapper.UsuarioMapper;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {
  
  private UsuarioMapper mapper;
  
  @Autowired
  public void setMapper(UsuarioMapper mapper) {
    this.mapper = mapper;
  }

  @Override
  public void salvar(Usuario usuario) {
    mapper.salvar(usuario);
  }

  @Override
  public Usuario getUsuarioPorLoginESenha(Usuario usuario) {
    return mapper.getUsuarioPorLoginESenha(usuario);
  }

  @Override
  public void atualizar(Usuario usuario) {
    mapper.atualizar(usuario);
  }
 
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException,
      DataAccessException {
      return mapper.usuarioPeloLogin(username);
  }

}
