package br.com.funhouse.security;

import br.com.funhouse.liga.Time;


public class Usuario {
  
  private Integer id;
  private Integer administrador;
  private String apelido;
  private String login;
  private String senha;
  private Time time;
  
  public Integer getId() {
    return id;
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  public String getApelido() {
    return apelido;
  }
  
  public void setApelido(String apelido) {
    this.apelido = apelido;
  }
  
  public String getLogin() {
    return login;
  }
  
  public void setLogin(String login) {
    this.login = login;
  }
  
  public String getSenha() {
    return senha;
  }
  
  public void setSenha(String senha) {
    this.senha = senha;
  }

  public Time getTime() {
    return time;
  }

  public void setTime(Time time) {
    this.time = time;
  }

  public Integer getAdministrador() {
    return administrador;
  }

  public void setAdministrador(Integer administrador) {
    this.administrador = administrador;
  }

  
  
}
