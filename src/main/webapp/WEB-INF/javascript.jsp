<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="<c:url value='/public/javascript/jquery-1.6.2.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/jquery.xml.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/jquery.maskedinput-1.3.min.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/jquery.validate.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/jquery.gsh.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/additional-methods.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/jquery.formatters.js'/>"></script>
<script type="text/javascript" src="<c:url value='/public/javascript/messages_ptbr.js'/>"></script>
<script type="text/javascript" src='<c:url value="/public/javascript/jquery.dataTables.js" />'></script>
<script type="text/javascript" src='<c:url value="/public/javascript/jquery.price_format.1.0.js" />'></script>
<script type="text/javascript" src='<c:url value="/public/javascript/main.js" />'></script>
<script type="text/javascript" src='<c:url value="/public/javascript/jquery.maskedinput-1.1.4.pack.js" />'></script>
<script type="text/javascript" src='<c:url value="/public/javascript/jquery.maskMoney.js" />'></script>
</head>