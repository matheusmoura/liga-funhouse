<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <link rel="stylesheet" href="<c:url value="/public/css/bootstrap.css"/>">
    <link rel="stylesheet" href='<c:url value="/public/css/demo_table_jui.css"/>' media="screen" type="text/css" />
    <title>Liga Funhouse! v0.1</title>
  </head>
  <body>
  <table>
  <tr>
    <td>
      <div>
        <ul class="pills">  
          <li>
             <a href="<c:url value='/time/${session.time.id}'/>">Home</a>
          </li>  
          <li>
             <a href="<c:url value = '/usuario/index'/>">Alterar Senha</a>
          </li>  
          <li>
             <a href="<c:url value='/logout'/>">Logout</a>
          </li>
          <li>
             <a href="<c:url value='/administrativo/index'/>">Administrativo</a>
          </li>
          <c:if test="${statusMercado == 'EM LEILÃO'}" >
            <li>
               <a href="<c:url value='/leilao/index'/>">Leilão</a>
            </li>
          </c:if>
          <c:if test="${statusMercado == 'FINALIZADO'}" >
            <li class="active">
               <a href="<c:url value='/time/listar'/>">Listagem de Times</a>
            </li>
          </c:if> 
        </ul>  
      </div>
    </td>
  <td><h3>O mercado está: <strong><h2><i><c:out value = "${statusMercado}"/></i><h2></strong><h3></td>
  <td><img src="<c:url value="/public/img/${emblema}"/>" width="80px" height="80px"/></td>
  </tr>
  </table>
    
    <c:if test="${notice != null}">
      <div class="alert-message success">
          <p><strong>${notice}</strong></p>
      </div>
    </c:if>
    <c:if test="${errors != null}">
      <div class="alert-message error">
        <c:forEach var="error" items="${errors}">
           <c:out value="${error.message}"/>
        </c:forEach>
      </div>
    </c:if>  
    <form id="pesquisa" action="<c:url value="/time/pesquisa"/>" method="post">
      <div>
        <c:out value="Time: "/>
        <select name="time.id">
          <option value="99"> ESCOLHA: </option>
		        <c:forEach items="${times}" var="time">
		           <option value="${time.id}"> ${time.nome}</option>
		        </c:forEach>
        </select>
      </div>
      <p></p>
      <div>
       <input id="submit" class="btn primary" type="submit" value="Enviar"/>
      </div>
    </form>
    <table id="tabelaJogadores" class="display">
      <thead>
        <tr>
          <th>TIME DA LIGA</th>
          <th>JOGADOR</th>
          <th>TIME</th>
          <th>POSIÇÃO</th>
          <th>ARREMATE NO LEILÃO</th>
        </tr>
      </thead>
      <tbody>
        <c:forEach var="jogador"  items="${time.jogadores}">
          <tr>
            <td>${time.nome}</td>
            <td>${jogador.nome}</td>
            <td>${jogador.nomeTime}</td>
            <td>${jogador.posicao}</td>
            <td>${jogador.valorLeilao}</td>
          </tr>
        </c:forEach>
      </tbody>
    </table>
    <jsp:include page="/WEB-INF/javascript.jsp" />
    <script type="text/javascript">
      $(function(){
          $('#tabelaJogadores').dataTable();
      });
    </script>
  </body>
</html>