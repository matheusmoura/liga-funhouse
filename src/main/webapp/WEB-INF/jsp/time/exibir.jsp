<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='security' uri='http://www.springframework.org/security/tags' %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
	<head>
		<link rel="stylesheet" href='<c:url value="/public/css/demo_table_jui.css"/>' media="screen" type="text/css" />
		<link rel="stylesheet" href="<c:url value="/public/css/bootstrap.css"/>">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Liga Funhouse! v0.1</title>
	</head>
  <body>
  <table>
	  <tr>
	    <td>
	      <div>
	        <ul class="pills">  
	          <li  class="active">
	             <a href="<c:url value='/time/${session.time.id}'/>">Home</a>
	          </li>  
	          <li>
	             <a href="<c:url value = '/usuario/index'/>">Alterar Senha</a>
	          </li>  
	          <li>
	             <a href="<c:url value='/logout'/>">Logout</a>
	          </li>
	          <li>
	             <a href="<c:url value='/administrativo/index'/>">Administrativo</a>
	          </li>
	          <c:if test="${statusMercado == 'EM LEILÃO'}" >
              <li>
                 <a href="<c:url value='/leilao/index'/>">Leilão</a>
              </li>
            </c:if> 
	          <c:if test="${statusMercado == 'FINALIZADO'}" >
              <li>
                 <a href="<c:url value='/time/listar'/>">Listagem de Times</a>
              </li>
            </c:if> 
	        </ul>  
	      </div>
	    </td>
	  <td><h3>O mercado está: <strong><h2><i><c:out value = "${statusMercado}"/></i><h2></strong><h3></td>
	  <td><img src="<c:url value="/public/img/${emblema}"/>" width="80px" height="80px"/></td>
	  </tr>
  </table>
	  <div>
      <div><strong><c:out value="Olá ${session.apelido}!"/></strong></div>
      <p/>
      <div><strong><c:out value="Seu saldo é de: F$ ${time.conta.saldo}"/></strong></div>
    </div>
    <p/>
		<c:if test="${error != null}">
			<div class="alert-message error">
	        <a class="close" href="#">×</a>
	        <p><strong>${error}</strong></p>
	    </div>
		</c:if>
		<c:if test="${notice != null}">
			<div class="alert-message success">
	        <a class="close" href="#">×</a>
	        <p><strong>${notice}</strong></p>
	    </div>
		</c:if>
		<div>
		<table id="tabelaJogadores" class="display">
			<thead>
				<tr>
					<th>JOGADOR</th>
					<th>POSIÇÃO</th>
					<th>VALOR</th>
					<th>UTILIZADO NO LEILÃO</th>
					<th>AÇÃO</th>
				</tr>
			</thead>
			<tbody>
  			<c:forEach var="jogador"  items="${time.jogadores}">
					<tr>
						<td>${jogador.nome}</td>
						<td>${jogador.posicao}</td>
						<td>${jogador.preco}</td>
						<td>${jogador.valorLeilao}</td>
						<td>
						  <c:if test="${statusMercado == 'ABERTO'}">
						    <a href="<c:url value='/time/excluir/jogador/${jogador.id}' />">Excluir</a>
						  </c:if>
						  <c:if test="${statusMercado == 'REFUGOS' && jogador.refugo == 1}">
                <a href="<c:url value='/time/excluir/jogador/${jogador.id}' />">Excluir</a>
              </c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
		<p>
		</p>
		<c:if test="${statusMercado == 'ABERTO' || statusMercado == 'REFUGOS'}">
			<div align="left" >
			  <a href="<c:url value="/jogadores/"/>">INCLUSÃO DE JOGADOR</a> 
			</div>
		</c:if>
			<p/>
		  <c:if test="${statusMercado != 'ABERTO'}">
				<div>
				  <h3>RESULTADOS DO LEILÃO</h3>
				</div>
				<div>
				 <table>
				   <c:forEach var="notificacao"  items="${notificacoes}">
	          <tr>
	            <td>${notificacao.mensagem}</td>
	            <td><fmt:formatDate value="${notificacao.data}" pattern="dd/MM/yyyy HH:mm:ss" /> </td>
	          </tr>
	        </c:forEach>
				 </table>
				</div>
			 </c:if>
		<jsp:include page="/WEB-INF/javascript.jsp" />
		<script type="text/javascript">
			$(function(){
			    $('#tabelaJogadores').dataTable({
			    	 "aaSorting": [[ 3, "desc" ]]
			    });
			});
		</script>
	</body>
</html>