<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
	<head>
		<link rel="stylesheet" href="<c:url value="/public/css/bootstrap.css"/>">
		<link rel="stylesheet" href='<c:url value="/public/css/demo_table_jui.css"/>' media="screen" type="text/css" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Liga Funhouse! v0.1</title>
	</head>
  <body>
  <table>
    <tr>
      <td>
        <div>
          <ul class="pills">  
            <li class="active">
               <a href="<c:url value='/time/${session.time.id}'/>">Home</a>
            </li>  
            <li>
               <a href="<c:url value = '/usuario/index'/>">Alterar Senha</a>
            </li>  
            <li>
               <a href="<c:url value='/logout'/>">Logout</a>
            </li>
            <li>
               <a href="<c:url value='/administrativo/index'/>">Administrativo</a>
            </li>
            <c:if test="${statusMercado == 'EM LEILÃO'}" >
	            <li>
	               <a href="<c:url value='/leilao/index'/>">Leilão</a>
	            </li>
            </c:if>
            <c:if test="${statusMercado == 'FINALIZADO'}" >
             <li>
                <a href="<c:url value='/time/listar'/>">Listagem de Times</a>
             </li>
           </c:if> 
          </ul>  
        </div>
      </td>
    <td><h3>O mercado está: <strong><h2><i><c:out value = "${statusMercado}"/></i><h2></strong><h3>
    </td>
    <td><img src="<c:url value="/public/img/${emblema}"/>" width="80px" height="80px"/></td>
    </tr>
  </table>
  <div>
      <div><strong><c:out value="Usuário: ${session.apelido}"/></strong></div>
      <p/>
      <div><strong><c:out value="Saldo: F$ ${time.conta.saldo}"/></strong></div>
  </div>
  <p></p>
		<form id="pesquisa" action="<c:url value="/jogadores/pesquisar"/>" method="post">
      <div>
        <c:out value="Nome do Jogador:"/><input type="text" name="jogador.nome"/>
      </div>
      <p></p>
      <div>
        <c:out value="Posicao: "/>
        <select name="jogador.posicao">
	         <option value=""> ESCOLHA: </option>
	         <option value="GK"> Goleiro (GK) </option>
	         <option value="LB"> Lateral Esquerdo (LB) </option>
	         <option value="RB"> Lateral Direito (RB) </option>
	         <option value="CB"> Zagueiro (CB) </option>
	         <option value="SW"> Líbero (SW) </option>
	         <option value="DMF"> Volante (DMF) </option>
	         <option value="LMF"> Meia Esquerdo (LMF) </option>
	         <option value="RMF"> Meia Direito (RMF) </option>
	         <option value="CMF"> Meia Central (CMF) </option>
	         <option value="AMF"> Meia-Atacante (AMF) </option>
	         <option value="LWF"> Ponta Esquerda (LWF) </option>
	         <option value="RWF"> Ponta Direita (RWF) </option>
	         <option value="SS"> Segundo Atacante (SS) </option>
	         <option value="CF"> Centro-Avante (CF) </option>
        </select>
      </div>
      <p></p>
      <div>
       <input id="submit" class="btn primary" type="submit" value="Pesquisar"/>
      </div>
    </form>
		<c:if test="${jogadores != null}">
		<table id="tabelaJogadores" class="display">
			<thead>
				<tr>
					<th>JOGADOR</th>
					<th>TIME</th>
					<th>POSIÇÃO</th>
					<th>VALOR</th>
					<th>AÇÃO</th>
				</tr>
			</thead>
			<tbody>
  			<c:forEach var="jogador"  items="${jogadores}">
					<tr>
						<td>${jogador.nome}</td>
						<td>${jogador.nomeTime}</td>
						<td>${jogador.posicao}</td>
						<td>${jogador.preco}</td>
						<td>
  					  <c:if test="${statusMercado == 'ABERTO'}">
	  					  <form action="<c:url value="/time/incluir/jogador/${jogador.id}"/>" method="POST">
	  					     <input type="hidden" value="${jogador.id}" name="jogador.id"/> 
	  					     <input type="submit" class="btn primary" value = "Incluir" id="incluir"/>
	  					  </form>
  					  </c:if>
  					  <c:if test="${statusMercado == 'REFUGOS'}">
  					   <form action="<c:url value="/time/incluir/jogador/refugos/${jogador.id}"/>" method="POST">
                   <input type="hidden" value="${jogador.id}" name="jogador.id"/> 
                   <input type="submit" class="btn primary" value = "Incluir" id="incluir"/>
                </form>
  					  </c:if>
  					</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		</c:if>
		<jsp:include page="/WEB-INF/javascript.jsp" />
		<script type="text/javascript">

		$(document).ready(function(){
			$("input#incluir").click(function() {
				var url = $('input[type="hidden"]').val();
					//$('a').attr('href');
			});
		});
			$(function(){
				    $('#tabelaJogadores').dataTable({
				    	 "aaSorting": [[ 4, "desc" ]]
				    });
			});
		</script>
	</body>
</html>