<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<link rel="stylesheet" href="<c:url value="/public/css/bootstrap.css"/>">
<link rel="stylesheet" href='<c:url value="/public/css/demo_table_jui.css"/>' media="screen" type="text/css" />
<link rel="stylesheet" href='<c:url value="/public/css/main.css"/>' media="screen" type="text/css" />
<head>
</head>
<body><c:out value="${times}"></c:out>
<form id="pesquisa" action="<c:url value="/administrativo/mercado"/>" method="post">
     <div>
       <c:out value="Mercado: "/>
       <select name="status">
          <option value="99"> ESCOLHA: </option>
          <option value="0"> Aberto </option>
          <option value="1"> Em Leilão </option>
          <option value="2"> Refugos </option>
          <option value="3"> Finalizado </option>
       </select>
     </div>
     <p></p>
     <div>
      <input id="submit" class="btn primary" type="submit" value="Enviar"/>
     </div>
   </form>
</body>