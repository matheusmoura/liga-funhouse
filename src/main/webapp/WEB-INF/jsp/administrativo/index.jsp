<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <link rel="stylesheet" href="<c:url value="/public/css/bootstrap.css"/>">
    <link rel="stylesheet" href='<c:url value="/public/css/demo_table_jui.css"/>' media="screen" type="text/css" />
    <link rel="stylesheet" href='<c:url value="/public/css/main.css"/>' media="screen" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Liga Funhouse! v0.1</title>
  </head>
  <body>
  <table>
  <tr>
	  <td>
	    <div>
	      <ul class="pills">  
	        <li>
	           <a href="<c:url value='/time/${session.time.id}'/>">Home</a>
	        </li>  
	        <li>
	           <a href="<c:url value = '/usuario/index'/>">Alterar Senha</a>
	        </li>  
	        <li>
	           <a href="<c:url value='/logout'/>">Logout</a>
	        </li>
	        <li class="active">
	           <a href="<c:url value='/administrativo/index'/>">Administrativo</a>
	        </li>
	        <c:if test="${statusMercado == 'EM LEILÃO'}" >
            <li>
               <a href="<c:url value='/leilao/index'/>">Leilão</a>
            </li>
          </c:if>
          <c:if test="${statusMercado == 'FINALIZADO'}" >
            <li>
               <a href="<c:url value='/time/listar'/>">Listagem de Times</a>
            </li>
          </c:if> 
	      </ul>  
	    </div>
	  </td>
  <td><h3>O mercado está: <strong><h2><i><c:out value = "${statusMercado}"/></i><h2></strong><h3></td>
  </tr>
  </table>
    
    <c:if test="${notice != null}">
      <div class="alert-message success">
          <p><strong>${notice}</strong></p>
      </div>
    </c:if>
    <c:if test="${errors != null}">
      <div class="alert-message error">
        <c:forEach var="error" items="${errors}">
           <c:out value="${error.message}"/>
        </c:forEach>
      </div>
    </c:if>  
	      <div id="page">
	          <ul id="tabs" class="mytabs">
	               <li class="current"><a href="<c:url value="/administrativo/tabs/tab1"/>">Mercado</a></li>
	               <li><a href="<c:url value="/administrativo/tabs/tab2"/>">Caixa de times</a></li>
	          </ul>
	          <div id="tabs-container" class="mytabs-container">
	               Carregando. Aguarde por favor...
	          </div>
	      </div>
    <jsp:include page="/WEB-INF/javascript.jsp" />
  </body>
</html>