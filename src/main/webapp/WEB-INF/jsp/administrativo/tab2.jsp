<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
</head> 
	<body>
		<form action="<c:url value="/time/editar"/>" method="post">
		    <table id="tabela" class="display">
		      <thead>
		        <tr>
		          <th>TIME</th>
		          <th>CAIXA</th>
		        </tr>
		      </thead>
		      <tbody>
		        <c:forEach var="time"  items="${times}" varStatus="i">
		          <tr>
		            <td>${time.nome}</td>
		            <input type="hidden" name="times[${i.index}].id" value = "${time.id}"/>
		            <td><input type="text" name="times[${i.index}].caixa" value="${time.caixa}" maxlength="10"/> </td>
		          </tr>
		        </c:forEach>
		      </tbody>
		    </table>
		    <p/>
		    <input type="submit" id="submit" class="btn primary"  value="Enviar"/>
		</form>
	</body>
	  <script type="text/javascript">
      $(function(){
          $('#tabela').dataTable({
        		"bPaginate": false
        	});
      });
    </script>
</html>