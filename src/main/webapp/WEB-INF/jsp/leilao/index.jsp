<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
  <head>
    <link rel="stylesheet" href="<c:url value="/public/css/bootstrap.css"/>">
    <link rel="stylesheet" href='<c:url value="/public/css/demo_table_jui.css"/>' media="screen" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Liga Funhouse! v0.1</title>
  </head>
  <body>
  <table>
  <tr>
    <td>
      <div>
        <ul class="pills">  
          <li>
             <a href="<c:url value='/time/${session.time.id}'/>">Home</a>
          </li>  
          <li>
             <a href="<c:url value = '/usuario/index'/>">Alterar Senha</a>
          </li>  
          <li>
             <a href="<c:url value='/logout'/>">Logout</a>
          </li>
          <li >
             <a href="<c:url value='/administrativo/index'/>">Administrativo</a>
          </li>
          <c:if test="${statusMercado == 'EM LEILÃO'}" >
            <li class="active">
               <a href="<c:url value='/leilao/index'/>">Leilão</a>
            </li>
          </c:if>
          <c:if test="${statusMercado == 'FINALIZADO'}" >
             <li>
                <a href="<c:url value='/time/listar'/>">Listagem de Times</a>
             </li>
          </c:if> 
        </ul>  
      </div>
    </td>
  <td><h3>O mercado está: <strong><h2><i><c:out value = "${statusMercado}"/></i><h2></strong><h3></td>
  <td><img src="<c:url value="/public/img/${emblema}"/>" width="80px" height="80px"/></td>
  </tr>
  </table>
    
    <c:if test="${notice != null}">
      <div class="alert-message success">
          <p><strong>${notice}</strong></p>
      </div>
    </c:if>
    <c:if test="${error != null}">
      <div class="alert-message error">
          <p><strong>${error}</strong></p>
      </div>
    </c:if>
    <div>
      <div><strong><c:out value="Usuário: ${session.apelido}"/></strong></div>
      <p/>
      <div><strong><c:out value="Saldo: F$ ${saldo}"/></strong></div>
    </div>
    <p/>
    <form id="pesquisa" action="<c:url value="/leilao/enviar"/>" method="post">
    <div id = "tabela">
	    <table id="tabelaJogadores" class="display">
	      <thead>
	        <tr>
	          <th>JOGADOR</th>
	          <th>ARREMATE</th>
	        </tr>
	      </thead>
	      <tbody>
	        <c:forEach var="leilao"  items="${jogadoresEmLeilao}" varStatus="i">
	          <tr>
	            <td>${leilao.nomeJogador}</td>
	            <input type="hidden" name="leilao[${i.index}].idJogador" value = "${leilao.idJogador}"/>
	            <td><input type="text" name="leilao[${i.index}].lance" value="${leilao.lance}" maxlength="10"/></td>
	          </tr>
	        </c:forEach>
	      </tbody>
	    </table>
    </div>
       <div align="center">
         <input type="submit" value="Enviar" class="btn primary">
       <div>
    </form>
    <jsp:include page="/WEB-INF/javascript.jsp" />
    <script type="text/javascript">
    $(function(){
          $("input[type=text]").each(function(){
	        	  $(this).maskMoney({showSymbol:true, symbol:"", decimal:".", thousands:""});
        	  });
          $('#tabelaJogadores').dataTable();
      });
    </script>
  </body>
</html>