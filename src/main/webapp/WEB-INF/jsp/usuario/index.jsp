<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='security' uri='http://www.springframework.org/security/tags' %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="/WEB-INF/javascript.jsp" />
<link rel="stylesheet" href="http://twitter.github.com/bootstrap/1.3.0/bootstrap.min.css"/>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Liga Funhouse! v0.1</title>
</head>
<body>
<div>
  <table>
	  <tr>
	    <td>
	      <div>
	        <ul class="pills">  
	          <li>
	             <a href="<c:url value='/time/${session.time.id}'/>">Home</a>
	          </li>  
	          <li class="active">
	             <a href="<c:url value = '/usuario/index'/>">Alterar Senha</a>
	          </li>  
	          <li>
	             <a href="<c:url value='/logout'/>">Logout</a>
	          </li>
	          <li>
	             <a href="<c:url value='/administrativo/index'/>">Administrativo</a>
	          </li>
	          <c:if test="${statusMercado == 'EM LEILÃO'}" >
              <li>
                 <a href="<c:url value='/leilao/index'/>">Leilão</a>
              </li>
            </c:if>
            <c:if test="${statusMercado == 'FINALIZADO'}" >
             <li>
                <a href="<c:url value='/time/listar'/>">Listagem de Times</a>
             </li>
            </c:if> 
	        </ul>  
	      </div>
	    </td>
	  <td><h3>O mercado está: <strong><h2><i><c:out value = "${statusMercado}"/></i><h2></strong><h3></td>
	  <td><img src="<c:url value="/public/img/${emblema}"/>" width="80px" height="80px"/></td>
	  </tr>
  </table>
    <c:if test="${notice != null}">
	    <div class="alert-message success">
	        <c:out value="${notice}" />
	    </div>
  	</c:if>

  	<c:if test="${errors != null}">
	   	<div class="alert-message error">
		    <c:forEach var="error" items="${errors}">
		       <c:out value="${error.message}"/>
		    </c:forEach>
	    </div>
  	</c:if> 
<p/>
<c:out value="Usuário: "></c:out> <b>${session.apelido}</b> 
<p></p>
</div>
    <form action="<c:url value="/usuario/alterarSenha" />" method="POST">
     <input type="hidden" name="_method" value="PUT"/>
      <c:out value="Nova senha: "></c:out><input type="password" name="usuario.senha"/>
      <p></p>
      <input type="submit" class="btn primary" value="Enviar"> 
    </form>
</body>
</html>