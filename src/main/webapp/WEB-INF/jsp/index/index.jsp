<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c"        uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix='security' uri='http://www.springframework.org/security/tags'%>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Liga Funhouse! v0.1</title>
	</head>
  <body>
		<c:if test="${error != null}">
			<li><font color="red"><c:out value="${error}" />
			</font>
			</li>
		</c:if>
		<div id="indexForm">
			<form class="pull-right" action="<c:url value='/autenticar'/>" method="POST">
				<div>
					Login: <input type='text' name='usuario.login' placeholder="Usuário" class="input span2" />
				</div>
				<div>
					Senha: <input type='password' name='usuario.senha' placeholder="Senha" class="input span2" />
				</div>
				<button type="submit" class="btn">Enviar</button>
			</form>
		</div>
  </body>
</html>