(function($) {
  $.validator.setDefaults({
    errorPlacement: function(error, element) {
      error.appendTo(element.parent());
    },
    errorElement: "span",
    highlight:function (element, errorClass, validClass){
      $($(element).parent()).parent().addClass(errorClass);
    },
    unhighlight:function (element, errorClass, validClass){
      $($(element).parent()).parent().removeClass(errorClass);
    }
  });
  
  var that = {};
  that.httpStatus = {
      100 : "Continue",
      101 : "Switching Protocols",
      200 : "OK",
      201 : "Created",
      202 : "Accepted",
      203 : "Non-authoritative Information",
      204 : "No Content",
      205 : "Reset Content",
      206 : "Partial Content",
      300 : "Multiple Choices",
      301 : "Moved Permanently",
      302 : "Found",
      303 : "See Other",
      304 : "Not Modified",
      305 : "Use Proxy",
      307 : "Temporary Redirect",
      400 : "Bad Request",
      401 : "Unauthorized",
      402 : "Payment Required",
      403 : "Forbidden",
      404 : "Not Found",
      405 : "Method Not Allowed",
      406 : "Not Acceptable",
      407 : "Proxy Authentication Required",
      408 : "Request Timeout",
      409 : "Conflict",
      410 : "Gone",
      411 : "Length Required",
      412 : "Precondition Failed",
      413 : "Request Entity Too Large",
      414 : "Request-url Too Long",
      415 : "Unsupported Media Type",
      417 : "Expectation Failed",
      500 : "Internal Server Error",
      501 : "Not Implemented",
      502 : "Bad Gateway",
      503 : "Service Unavailable",
      504 : "Gateway Timeout",
      505 : "HTTP Version Not Supported"
  };
  that.updateStatus = function (req, textStatus) {
    if (req.status) {
      $("#statusCode").val(req.status + " (" + that.httpStatus[req.status] + ")");
    }
  };
  that.mostraResultado = function (data) {
    var text = data;
    if (jQuery.isXMLDoc(data)) {
      text = $(data).xml("all");
    }
    $("#result").val(text);
  };
  that.badRequest = function (errors) {
    if (errors) {
      $("#result").val(errors.responseText);
    }
  };
  that.internalError = function (error) {
    if (error) {
      $("#result").val(error.responseText);
    }
  };
  that.capturaDados = function (selector) {
    var data = {};
    if (selector) {
      $(selector).each(function() {
        data[this.name] = $(this).val();
      });
    }
    return data;
  };
  that.submit = function (settings) {
      $.ajax({
        url : (function(){
         return (typeof settings.url === "function") ? settings.url() : settings.url;   
        })(),
        success : that.mostraResultado,
        statusCode : {
          400 : that.badRequest,
          500 : that.internalError
        },
        dataType : settings.dataType,
        data : that.capturaDados(settings.dataSelector),
        type : settings.method,
        complete : settings.complete
      });
  };
  that.methods = {
    init : function (options) {
      return that.methods.submit(options);
    },
    submit : function(options) {
      var settings = {
        url : "/",
        dataSelector : $("#form *").filter(":input").filter("[type!=reset]").filter("[type!=submit]"),
        method : "GET",
        complete : that.updateStatus,
        dataType : "text",
        validations: function(){ return true; }
      };

      if (options) {
        $.extend(settings, options);
      }
      $("#form").validate({
        submitHandler: function(form) {  
          that.submit(settings); 
          return false;
        }
      });
      return this;
    },
    destroy : function() {
      return this;
    }
  };

  $.fn.gsh = function(method) {
    if (that.methods[method]) {
      return that.methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return that.methods.init.apply(this, arguments);
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.gsh');
    }
  };
})(jQuery);