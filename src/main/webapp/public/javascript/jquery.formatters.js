(function($) {
  var that = {};
  that.methods = {
    datetime : function () {
      this.each(function(){
        $(this).mask("99/99/9999 99:99:99");
      });
    },
    cpf: function() {
    	 this.each(function(){
    	        $(this).mask("999.999.999-99");
    	      });
	},
	cnpj: function() {
		this.each(function(){
			$(this).mask("99.999.999/9999-99");
		});
	},
	cep: function() {
		this.each(function(){
			$(this).mask("99999-999");
		});
	}
  };

  $.fn.formatters = function(method) {
    if (that.methods[method]) {
      return that.methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
    } else if (typeof method === 'object' || !method) {
      return that.methods.init.apply(this, arguments);
    } else {
      $.error('Method ' + method + ' does not exist on jQuery.gsh');
    }
  };
})(jQuery);