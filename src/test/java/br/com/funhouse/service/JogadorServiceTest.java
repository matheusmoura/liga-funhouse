package br.com.funhouse.service;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.JogadorService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
    "classpath:applicationContext.xml",
    "classpath:applicationContext-per*.xml" })
public class JogadorServiceTest {
  
  private JogadorService service;

  @Autowired
  public void setService(JogadorService service) {
    this.service = service;
  }
  
  @Test
  public void deveriaTrazerTodosJogadoresComNomeCristiano(){
    Jogador jogador = new Jogador();
    jogador.setNome("CRISTIANO");
    List<Jogador> jogadores = service.listJogadores(jogador);
    assertNotNull(jogadores);
  }
  
  @Test
  public void deveriaTrazerOTimeDoJogadorCristiano(){
    Jogador jogador = new Jogador();
    jogador.setNome("%CRISTIANO%");
    List<Jogador> jogadores = service.listJogadores(jogador);
    jogador = jogadores.get(0);
    assertEquals(jogador.getNomeTime(), "PAOK F.C. ");
  }
  
  @Test
  public void deveriaTrazerUmaListDeJogadoresPorPosicao(){
    Jogador jogador = new Jogador();
    jogador.setPosicao("%CMF%");
    List<Jogador> jogadores = service.listJogadores(jogador);
    assertNotNull(jogadores);
  }
  
  @Test
  public void deveriaObterJogadorComPosicaoCMF(){
    Jogador jogador = new Jogador();
    jogador.setPosicao("%CMF%");
    List<Jogador> jogadores = service.listJogadores(jogador);
    jogador = jogadores.get(0);
    assertEquals(jogador.getPosicao(), " CMF");
  }
  
}
