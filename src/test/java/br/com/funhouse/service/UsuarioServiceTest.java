package br.com.funhouse.service;

import static org.junit.Assert.assertEquals;

import java.io.FileInputStream;
import java.sql.Connection;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.UsuarioService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
    "classpath:applicationContext.xml",
    "classpath:applicationContext-per*.xml" })
public class UsuarioServiceTest {
  
  private DataSource dataSource;
  private FlatXmlDataSetBuilder builder;
  
  private UsuarioService service;
  
  @Autowired
  public void setService(UsuarioService service) {
    this.service = service;
  }

  @Autowired
  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
  
  @Before
  public void init() throws Exception {
    DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
  }
  
  @After
  public void after() throws Exception {
    DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
  }
  
  private IDataSet getDataSet() throws Exception {
    builder = new FlatXmlDataSetBuilder();
    return builder.build(new FileInputStream("src/test/resources/usuario.xml"));
  }
  
  private IDatabaseConnection getConnection() throws Exception {
    Connection con = dataSource.getConnection();
    IDatabaseConnection connection = new DatabaseConnection(con);
    DatabaseConfig config = connection.getConfig();
    config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
    return connection;
  }
  
  @Test
  public void deveriaObterOGordo(){
    Usuario usuario = new Usuario();
    usuario.setLogin("dope");
    usuario.setSenha("12345");
    usuario = service.getUsuarioPorLoginESenha(usuario);
    assertEquals(usuario.getApelido(), "Gordo");
  }
  
  @Test
  public void deveriaObterOTimeDoGordo(){
    Usuario usuario = new Usuario();
    usuario.setLogin("dope");
    usuario.setSenha("12345"); 
    usuario = service.getUsuarioPorLoginESenha(usuario);
    assertEquals(Integer.valueOf(1),usuario.getTime().getId());
  }
  
  @Test
  public void deveriaAtualizarOGordo(){
    Usuario usuario = new Usuario();
    usuario.setLogin("dope");
    usuario.setSenha("12345"); 
    usuario = service.getUsuarioPorLoginESenha(usuario);
    usuario.setApelido("DOPE");
    service.atualizar(usuario);
    usuario = service.getUsuarioPorLoginESenha(usuario);
    assertEquals(usuario.getApelido(), "DOPE");
  }
  
}
