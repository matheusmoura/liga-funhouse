package br.com.funhouse.service;

import java.io.FileInputStream;
import java.sql.Connection;

import javax.sql.DataSource;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { 
    "classpath:applicationContext.xml",
    "classpath:applicationContext-per*.xml" })
public class MontagemServiceTeste {
  
  private DataSource dataSource;
  private FlatXmlDataSetBuilder builder;
  
//  private EscalacaoService service;
//  
//  @Autowired
//  public void setService(EscalacaoService service) {
//    this.service = service;
//  }

  @Autowired
  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }
  
  @Before
  public void init() throws Exception {
    DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
  }
  
  @After
  public void after() throws Exception {
    DatabaseOperation.DELETE_ALL.execute(getConnection(), getDataSet());
  }
  
  private IDataSet getDataSet() throws Exception {
    builder = new FlatXmlDataSetBuilder();
    return builder.build(new FileInputStream("src/test/resources/montagem.xml"));
  }
  
  private IDatabaseConnection getConnection() throws Exception {
    Connection con = dataSource.getConnection();
    IDatabaseConnection connection = new DatabaseConnection(con);
    DatabaseConfig config = connection.getConfig();
    config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new MySqlDataTypeFactory());
    return connection;
  }
  
  @Test
  public void deveriaObterUmTimeMontado(){
//    Escalacao escalacao = new Escalacao();
//    escalacao.setTime(new Time());
//    escalacao.getTime().setId(1);
//    List<Escalacao> listMontagens = service.listMontagens(escalacao);
//    assertNotNull(listMontagens);
  }
  
  @Test
  public void deveriaRetornarOTimeDoCabeca(){
//    Escalacao escalacao = new Escalacao();
//    escalacao.setTime(new Time());
//    escalacao.getTime().setId(1);
//    List<Escalacao> listMontagens = service.listMontagens(escalacao);
//    escalacao = listMontagens.get(0);
//    assertEquals(escalacao.getTime().getNome(), "Febem FC");
  }
  
}
