package br.com.funhouse.controller;

import java.util.List;

import org.junit.*;
import org.springframework.security.authentication.encoding.PasswordEncoder;

import static org.junit.Assert.*;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.Validator;
import br.com.caelum.vraptor.util.test.MockResult;
import br.com.caelum.vraptor.util.test.MockValidator;
import br.com.caelum.vraptor.validator.Message;
import br.com.caelum.vraptor.validator.ValidationException;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.UsuarioService;
import br.com.funhouse.security.session.UserSession;
import static org.mockito.Mockito.*;

public class UsuarioControllerTest {
  
  private UsuarioController controller;
  private UsuarioService service;
  private Usuario usuario;
  private UserSession session;
  private Validator validator;
  private Result result;
  private PasswordEncoder encoder;
  
  @Before
  public void setUp(){
    usuario = new Usuario();
    usuario.setSenha("AAA");
    
    encoder = mock(PasswordEncoder.class);
    
    session = mock(UserSession.class);
    when(session.getUser()).thenReturn(usuario);
    
    validator = new MockValidator();
    service = mock(UsuarioService.class);
    result = new MockResult();
//    controller = new UsuarioController(service, session, validator, result, encoder);
  }
  
  @Test
  public void senhaDeveriaEstarPopulada(){
    assertNotNull(usuario.getSenha());
  }
  
  @Test
  public void deveriaGravarUmUsuario(){
    controller.salvar(any(Usuario.class));
    verify(service).salvar(any(Usuario.class));
  }
  
  @Test
  public void deveriaChamarMetodoDeAtualizarParaModificarSenhaDoUsuario(){
    controller.alterarSenha(usuario);
    verify(service).atualizar(usuario);
  }
  
  
  @Test
  public void deveriaOcorrerErroCasoSenhaNaoInformadaAoAlterarSenha(){
    try {
      usuario.setSenha("");
      controller.alterarSenha(usuario);
      fail("Deveria ocorrer erro de valida��o na altera��o de senha!");
    }
    catch (ValidationException exception) {
      List<Message> errors = exception.getErrors();
      assertFalse(errors.isEmpty());
      assertEquals(1, errors.size());
      
      Message validationError = errors.get(0);
      assertEquals("senhaInvalida", validationError.getCategory());
    }
  }
  
}
