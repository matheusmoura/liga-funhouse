package br.com.funhouse.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.util.test.MockResult;
import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.Time;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.session.UserSession;

public class MontagemControllerTest {
  
//  private EscalacaoController controller;
  private Jogador jogador;
//  private EscalacaoService service;
//  private Escalacao escalacao;
  private UserSession userSession;
  private Usuario usuario;
  private Time time;
  private Result result;
  
  @Before
  public void setUp(){
//    service = mock(EscalacaoService.class);
    jogador = mock(Jogador.class);
    userSession = mock(UserSession.class);
    usuario = mock(Usuario.class);
//    escalacao = mock(Escalacao.class);
    time = mock(Time.class);
    result = new MockResult();
    
//    when(escalacao.getJogador()).thenReturn(jogador);
    when(usuario.getTime()).thenReturn(time);
    when(userSession.getUser()).thenReturn(usuario);
    
//    controller = new EscalacaoController(service, userSession, result);
    
  }
  
  @Test
  public void deveriaIncluirUmJogadorNaMontagem() throws Exception{
//    controller.incluir(jogador);
//    verify(service).adicionar(any(Escalacao.class));
  }
  
  @Test
  public void deveriaAdicionarOJogadorNaMontagem(){
//    controller.incluir(jogador);
//    assertNotNull(escalacao.getJogador());
  }
  
  @Test
  public void deveriaAdicionarUsuarioNaMontagem(){
//    controller.incluir(jogador);
//    verify(userSession).getUser();
  }
  
  @Test
  public void deveriaExcluirUmJogadorNaMontagem() throws Exception{
//    controller.excluir(escalacao);
//    verify(service).excluir(any(Escalacao.class));
  }
  
  @Test
  public void deveriaExibirMensagemDeSucessoAoExcluir(){
//    controller.excluir(escalacao);
//    String sucesso = (String) result.included().get("Sucesso");
//    assertEquals("Jogador exclu�do com sucesso!!", sucesso);
  }
  @Test
  public void deveriaAssociarUsuarioAMontagem(){
//    controller.excluir(escalacao);
//    verify(escalacao).setTime(any(Time.class));
  }
  
  @Test
  public void deveriaConterErroEmExcluirCasoMercadoFechado() throws Exception{
//    String excecaoEsperada = "Mercado fechado. N�o foi poss�vel excluir o jogador!";
//    doThrow(new Exception(excecaoEsperada)).when(service).excluir(escalacao);
//    controller.excluir(escalacao);
//    String erro = (String) result.included().get("erro");
//    assertEquals(excecaoEsperada, erro);
  }
}
