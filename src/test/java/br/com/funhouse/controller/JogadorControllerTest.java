package br.com.funhouse.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.util.test.MockResult;
import br.com.funhouse.liga.Jogador;
import br.com.funhouse.liga.JogadorService;
import br.com.funhouse.security.Usuario;
import br.com.funhouse.security.session.UserSession;
import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

public class JogadorControllerTest {
  
  private JogadorController controller;
  private UserSession session;
  private Result result;
  private Usuario usuario;
  private Jogador jogador;
  private JogadorService service;
  private List<Jogador> listJogadores;
  
  @Before
  public void setUp(){
    usuario = mock(Usuario.class);
    when(usuario.getLogin()).thenReturn("matheus");
    
    jogador = mock(Jogador.class);
    when(jogador.getNome()).thenReturn("CRISTIANO RONALDO");
    
    listJogadores = mock(List.class);
    
    when(listJogadores.get(0)).thenReturn(jogador);
    
    service = mock(JogadorService.class);
    when(service.listJogadores(jogador)).thenReturn(listJogadores);
    
    session = mock(UserSession.class);
    when(session.getUser()).thenReturn(usuario);
    
    result = new MockResult();
//    controller = new JogadorController(result, session, service);
    
    
  }
  
  @Test
  public void deveriaConterOUsuarioLogadoAoRedirecionarParaInclusaoDeJogador(){
//    controller.incluir();
//    Usuario user = (Usuario) result.included().get("usuario");
//    assertTrue("Deveria conter um usuario", user.getLogin().equals("matheus"));
  }
  
  @Test
  public void deveriaPesquisarUmJogadorPorNome(){
    controller.pesquisar(jogador);
    verify(service).listJogadores(jogador);
  }
  
  @Test
  public void deveriaRetornarUmJogadorNaPesquisa(){
    List<Jogador> list = service.listJogadores(jogador);
    assertNotNull(list);
  }
  
  @Test
  public void deveriaRetornarUmJogadorValidoNaPesquisa(){
    List<Jogador> list = service.listJogadores(jogador);
    assertEquals("CRISTIANO RONALDO", list.get(0).getNome());
  }
  
  @Test
  public void deveriaIncluirJogadorNoResult(){
    controller.pesquisar(jogador);
    List<Jogador> list = (List) result.included().get("listJogadores");
    assertNotNull("Deveria conter uma Lista de Jogadores", list);
  }
  
  @Test
  public void deveriaLancarMensagemDeErroCasoListaJogadorNula(){
    when(service.listJogadores(jogador)).thenReturn(null);
    controller.pesquisar(jogador);
    assertEquals(JogadorController.JOGADOR_NAO_ENCONTRADO, (String)result.included().get("error"));
  }
  
  @Test
  public void deveriaLancarMensagemDeErroCasoListaJogadorVazia(){
    when(service.listJogadores(jogador)).thenReturn(new ArrayList<Jogador>());
    controller.pesquisar(jogador);
    assertEquals(JogadorController.JOGADOR_NAO_ENCONTRADO, (String)result.included().get("error"));
  }
  
}
