package br.com.funhouse.controller;

import java.util.List;
import java.util.Map;

import org.junit.*;


import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.util.test.MockResult;
import br.com.funhouse.security.Usuario;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class LeilaoControllerTest {
 
  private Usuario usuario;
//  private List<Escalacao> listaMontagem;
//  private Escalacao escalacao;
  
  private Result result;
//  private EscalacaoService service;
  private LeilaoController controller;
  
  
  @Before
  public void setUp(){
//    service = mock(EscalacaoService.class);
    result = new MockResult();
//    controller = new LeilaoController(service, result);
    
    usuario = mock(Usuario.class);
//    escalacao = mock(Escalacao.class);
    
//    listaMontagem = mock(List.class);
//    listaMontagem.add(escalacao);
    
//    when(service.listJogadoresEmLeilao(usuario)).thenReturn(listaMontagem);
  }
  
  
  @Test
  public void deveriaChamarOMetodoDeListarOsJogadoresDoLeilaoDeUmTime(){
//    controller.listJogadoresEmLeilao();
//    verify(service).listJogadoresEmLeilao(any(Usuario.class));
  }
  
  @Test
  public void deveriaRetornarUmaListaDeJogadoresDoLeilaoDeUmTime(){
//    controller.listJogadoresEmLeilao();
//    assertNotNull(service.listJogadoresEmLeilao(usuario));
  }
  
  @Test
  public void deveriaInserirAListaDeJogadoresEmLeilaoNoResult(){
//    controller.listJogadoresEmLeilao();
//    assertNotNull(result.included().get("listJogadoresEmLeilao"));
  }
  
  @Test
  public void deveriaOcorrerErroCasoMercadoAberto(){
//    controller.listJogadoresEmLeilao();
//    assertNotNull(result.included().get("status"));
  }
  
}
